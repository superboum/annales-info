#include <unistd.h>
#include <stdio.h>

int main() {

  int i = 0;
  int j = 0;

  for (i = 1; i < 6; i++) {
    int pid = fork();

    if (pid == 0) {
      if (i == 1) {
        for (j = 1; j < 3; j++) {
          int pid2 = fork();
          if (pid2 == 0) {
            break;
          }
        }
      }
      break;
    }

  }

  printf("%d . %d\n", i == 6 ? 0 : i, j == 3 ? 0 : j);
  return 0;

}
