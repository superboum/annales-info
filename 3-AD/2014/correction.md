Correction Analyse de Données 2014
==================================

Exercice 1
----------

#### Question 1

- **Arbre de décision** : pour prendre une décision, on parcours un arbre. Chaque noeud est une condition qui mène à des feuilles différentes.
- **Machines à vecteurs de support** : on représente chaque élément par un vecteur, et on essaye de séparer les vecteurs grâce à un hyperplan
- **Réseaux de neurones** : l'activation de schéma de neurones virtuels est typique des classes apprises
- **Plus proches voisins** : classification automatique comme vu en cours

#### Question 2

- **Bagging** : de nombreux arbres sont générés aléatoirement. Le meilleur arbre est élu par l'ensemble.
- **Boosting** : à partir d'arbres très petits, le classifieur est capable de créer un arbre plus grand et plus performant

#### Question 3

On a à notre disposition 4 indicateurs, obtenus lors de la **validation croisée** : on apprends avec une partie des données, on teste ensuite avec le reste des données.

```
- Précision = bonnes prédictions / nombre de prédictions
- Rappel    = bonnes prédictions / nombre de prédictions attendues
- F-Meas    = 2*précision*rappel/(précision+rappel)
- Erreur    = mauvaises prédictions / nombre de prédictions
```

#### Question 4

Sur-apprentissage : le classifieur devient très bon sur le jeu d'apprentissage, mais créé des règles aberrantes en devenant trop spécifique à ce jeu. Face à un jeu de données inconnues, il ne pourra pas obtenir de bons résultats.

#### Question 5

Active learning : la machine demande à l'humain confirmation en cas de doute.

Exercice 2
----------

#### Question 1

- Relations entre individus
- Relations entre variables

#### Question 2

Oui, on a des écart-type très différents (protéines et sodium par exemple).

#### Question 3

Le rapport impliquera forcément protéines et lipides, il vaut mieux garder des variables actives à priori indépendantes pour réaliser l'ACP.

#### Question 4

Qualité : assez bonne sauf pour le Port-Salut (cf cosinus carrés). Les deux premiers axes peuvent suffire, mais il peut être intéressant de rajouter le troisième axe (on reste < 90% de pourcentage cumulé).

#### Question 5

- Inertie projetée = somme des v.p. = 2.7848
- Qualité de représentation axe 2 et 3 = 2.7848/11 = 0.25
- Qualité de représentation axe 1 et 3 = 7.3782/11 = 0.67

#### Question 6

Rétinol et vitB9 :

```
Voir poly 2.68

d²(a,b) = 2(1-corr(a,b))

d'où d²(rétinol,vitb9) = 2(1+0.11) = 2.22
```

Protéines et magnésium :

```
d²(protéines,magnésium) = 2(1-0.81) = 0.38
```

#### Question 7

a- sur l'origine grâce au centrage

b- voir poly 2.44, tableau 2, cases "distances à l'origine" et "coordonnées des individus actifs"

```
Exemple pour le Brie

QLT = ((-1.23)² + (1.58)²) / 4.97² = 0.16
```

c- voir poly 2.86, tableau 3, cases "corrélations des variables actives avec les facteurs"

```
Exemple pour Calories

QLT = 0.93² + 0.31² = 0.961
```

#### Question 8

*Lire cette réponse avec un accent de paysan*

Ces fromages viennent probablement de la même bestiole, ou ont des procédures d'affinage similaires.

#### Question 9

Les fromages sur la gauche des diagrammes sont plutôt sympas (vitamine et eau). Donc : chèvre frais, feta, langres.

Exercice 3
----------

#### Question 1

Le logiciel créé un hyperplan afin de pouvoir mesurer des distances convenables entre les différents individus. D'où le besoin de faire d'abord l'ACP.

#### Question 2

La distance de Ward prends en compte les **poids** de chaque classe, ainsi que leur barycentre. Comme l'inertie dépend de ces deux paramètres, on va pouvoir moduler l'inertie inter-classe de manière intelligente.

#### Question 3

Il s'agit de mesurer la similarité entre variables et classes. Cf poly 5.36 à 5.40.