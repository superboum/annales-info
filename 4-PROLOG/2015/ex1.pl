film(invictus,2009,eastwood,biographie,134).
film(gran_torino,2008,eastwood,drame,116).
film(interstellar,2014,nolan,science_fiction,169).
film(will_hunting,1997,van_sant,drame,121).
film(l_inspecteur_harry,1972,siegel,policier,102).
film(twin_peaks_fire_walk_with_me,1992,lynch,policier,135).
film(blue_velvet,1986,lynch,policier,120).
film(dune,1984,lynch,science_fiction,130).

acteur(eastwood,clint,1930,m,75).
acteur(damon,matt,1970,m,65).
acteur(freeman,morgan,1937,m,91).
acteur(hooper,dennis,1936,m,151).
acteur(mac_lachlan,kyle,1959,m,37).
acteur(rossellini,isabella,1952,f,45).

vedette(invictus,freeman,mandela).
vedette(invictus,damon,pienaar).
vedette(gran_torino,eastwood,kowalski).
vedette(interstellar,damon,dr_mann).
vedette(l_inspecteur_harry,eastwood,harry).
vedette(will_hunting,damon,hunting).
vedette(dune,mac_lachlan,atreides).
vedette(blue_velvet,mac_lachlan,beaumont).
vedette(blue_velvet,rossellini,vallens).
vedette(blue_velvet,hooper,booth).
vedette(twin_peaks_fire_walk_with_me,mac_lachlan,cooper).

%Question1
real_acteur(N,P,T) :-
  film(T,_,N,_,_),
  acteur(N,P,_,_,_),
  vedette(T,N,_).

%Question2
trois_vedettes(T,A) :-
  film(T,A,_,_,_),
  vedette(T,N1,_),
  vedette(T,N2,_),
  vedette(T,N3,_),
  N1 \= N2,
  N2 \= N3,
  N1 \= N3
  .

%Question3
pas_eastwood(T) :-
  film(T,_,E1,_,_),
  vedette(T,E2,_),
  E1 \= eastwood,
  E2 \= eastwood.

%Question4
pas_tous_policier_lynch(N) :-
  film(T,_,lynch,policier,_),
  not(vedette(T,N,_)).

tous_policier_lynch(N,P) :-
  acteur(N,P,_,m,_),
  not(pas_tous_policier_lynch(N)).

%Question5
genre_different(N) :-
  film(_,_,N,G1,_),
  film(_,_,N,G2,_),
  G1 \= G2.
genre_stable(N) :-
  film(_,_,N,_,_),
  not(genre_different(N)).



