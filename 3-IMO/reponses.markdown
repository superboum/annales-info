Annale 2014
===========

QCM
---

### Question 1

Cartographier le flux de valeur -> type de cartographie VSM

> Value Stream Mapping est une méthode de Lean manufacturing de cartographie d'un processus.

_Réponse C_

### Question 2

> Le takt time représente l’allure que la production doit avoir pour honorer le strict besoin client.

_Réponse B_

### Question 3

> Il faut produire à la demande sans délai à faible coût

> L’ensemble du personnel doit être impliqué afin que chacun puisse suggérer des pistes d’amélioration dans le cadre du progrès continu.

_Réponse B_

### Question 4

> Valeur Ajoutée = Recettes de vente - Dépenses d'achats

_Réponse B_

### Question 5

Poly Gestion Opérationnelle - Qualité. Page 56

3201 à 10 000 + Usage Généraux type II -> L  
Code L + Simple -> 200

_Réponse B_

### Question 6

Poly le progrès. Maintenabilité (p 62)
KPI de fiabilité, KPI de maintenabilité

> Les indicateurs clefs de performance (ICP), ou en anglais Key Performance Indicators (KPI) , sont des indicateurs mesurables d'aide décisionnelle.

_Réponse D (pas sûr)_

### Question 7

> Surproduction : produire en flux poussé, donc plus que le besoin et souvent trop tôt par rapport à la demande. Cette forme de gaspillage est la pire, puisqu'elle implique forcément les 6 autres types de gaspillages énumérés ci-dessus pour produire ce surplus.

_Réponse B_

### Question 8

 * SPC :  maîtrise statistique des procédés (MSP) (Statistical Process Control)
 * GPAO : Gestion de la production assistée par ordinateur
 * 5S : La méthode 5S est une approche systématique visant à améliorer la propreté et l'ordre dans l'environnement de travail. Elle peut être appliquée de l'atelier aux bureaux.
 * JAT : Juste à Temps

_Réponse C_

### Question 9

> Une gamme de fabrication est un document qui répertorie toutes les phases d'élaboration d'une pièce jusqu'à son stockage.

Une gamme de fabrication décrit comment réaliser l'object. Il est probable que l'on évoque ses pièces, mais ça ne semble pas être sa vocation. Rien n'est moins sûr, donc si vous avez d'autres infos...

_Réponse A ou B ou C ou D_

### Question 10

> NQA est l'abréviation de Niveau de Qualité Acceptable, (en anglais : AQL, Acceptable Quality Level).

_Réponse A_

### Question 11

> Méthode PDCA signifie: Plan, Do, Check, Adjust autrement appelée Roue de DEMING (ou cycle de SHEWHART, son inventeur).

Et pourtant c'est un extrait du polycopié... On va supposer que Act=Adjust ici.

_Réponse C_

### Question 12

Je ne sais pas

Selon Arnaud : Dans un atelier géré en Kanban, les lancements sont déterminés par les besoins effectifs à l’opération suivante

_Réponse D_

### Question 13

 * Les indicateurs liés à la dispersion: Cp et Pp
 * Les indicateurs liés au décentrage : Cpk et Cpm

_Réponse B_

### Question 14

_Réponse B_

### Question 15

Le Takt time. Cf ci-dessus

> Poka-Yoke: Un détrompeur (on trouve aussi le terme japonais poka-yoke, ポカヨケ, ou anti-erreur) 

_Réponse B_

### Question 16

![Graphe limite controle](limite-controle.png)

_Réponse D_

### Question 17

_Réponse C_

### Question 18

![Poly Goulot 1](goulot1.png)
![Poly Goulot 2](goulot2.png)

Après une petite incursion dans les flots de Quichaud (merci Arnaud et Quentin, pas de quoi etre fier...), j'ai bien envie de répondre A qui semble correspondre à la définition d'un arc saturé

_Réponse A (pas sûr)_

### Question 19

_Réponse B (pas sûr)_

### Question 20

Parce que fuck l'informatique, on est près des travailleurs #communistes.

Je ne suis pas sûr. J'ai choisi B car implication de l'équipe dans la démarche, et retours pour elles. On donne une idée de dynamisme, et on casse l'idée de hierarchie. On les implique dans leur travail. C'est très Lean où toute l'entreprise est impliquée + 5S qui est réalisé par les ouvriers directement aussi. On améliore sans avoir recourt à des personnes exterieures mais aux gens présents. C'est ce qui m'a fait pencher pour la C, qui est celle qui met l'humain au centre des préoccupations, blablabla, camaraderie, force rouge et vodka. La bise.

_Réponse C_

Exercice 1
----------

### Question 1

![Exercice 2 - Question 1](ex2-q1.png)

### Question 2

![Exercice 2 - Question 1](ex2-q2.png)
