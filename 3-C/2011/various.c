#include <stdio.h>
#include <stdlib.h>

void f4(int a[], int n) {
    a[n] += n;
}

void q16() {
    int t[] = {0,1,2}, i=1;
    f4(t,i);
    printf("Q16 : %d\n", t[i]);
}

void q17() {
    int x = 1, t[] = {1,2,3,9,10,11};
    x = *(t+t[0]+*(t));
    printf("Q17 : %d\n", x);
}

void f5(void) {
    static int i;
    printf("%d ", ++i);
}

void q18() {
    int i,j=4;
    char t[2] = {'O','K'};
    printf("Q18 : ");
    for(i=0;i<j;i++) {
        printf("%d %c %c ", ++i, t[0], *(t+1));
        f5();
    }
    printf("\n");
}

void q19() {
    unsigned int a=0x5C,A=0x01011100;
    unsigned int b=0xD1,B=0x11010001;
    unsigned int r,R;
    r= (a && b);
    printf("Q19: %8X", r);
    R= (A &  B);
    printf(" | %8X\n", R);
}

void q20() {
    int i,k;
    for(i=0,k=0;i<10;)
        k=i++;
    printf("Q20: %d\n", k);
}

void q21() {
    int i=0;
    printf("Q21: ");
    while(i==0) {
        printf("%d ", i);
        i++;
    }
    printf("%d\n", i);
}

void q22() {
    int i,j,k;
    for(i=0, k=0; i<=3; i++)
    for(j=i; j<=i; j++) { ++k; }
    printf("Q22: %d\n", k);
}

void q23() {
    int a=0,b=1,r;
    if(a == 0 && b == 0) r = 0;
    else if(a == 1 && b == 0 || a == 0 && b == 1) r = 1;
    else if(a == 1 && b == 1) r = 2;
    else r = 3;
    printf("Q23: %d\n", r);
}

void q24() {
    int a=1,b=0,r;
    switch(a+b) {
        case 0: r=0; // breaks are missing!
        case 1: r=1;
        case 2: r=2;
        default:r=3;
    }
    printf("Q24: %d\n", r);
}

void q25() {
    #define DMAX 5
    int tab[DMAX]={0};
    static int i;
    int *nb = &tab[0];  // *nb = tab
    while(i < DMAX-1) {
        (*nb)++;        // tab[0]++
        tab[*nb] = *nb; // tab[tab[0]] = tab[0]; 
        i++;
    }
    printf("Q25: ");
    for(i=0; i<DMAX; i++)
        printf("%d ", tab[i]);
    printf("\n");
}

void q26() {
    enum{INI,CLE,EXT,STT};
    int resul = INI;
    resul |= CLE | EXT | STT;
    printf("Q26: %d ", resul);
    resul &= ~(CLE | EXT | STT);
    printf("%d ", resul);
    resul |= CLE && EXT || STT;
    printf("%d\n", resul);
}

void q27() {
    typedef union {
        long adr;
        struct{
            unsigned char a,b,c,d,e;
        }x;
    }Ip;

    static Ip y;
    printf("Q27:\n");
    printf("%ld | %02X.%02X.%02X.%02X.%02X\n", y.adr,y.x.a,y.x.b,y.x.c,y.x.d,y.x.e);
    y.adr=1234567890;
    printf("%ld | %02X.%02X.%02X.%02X.%02X\n", y.adr,y.x.a,y.x.b,y.x.c,y.x.d,y.x.e);
    y.x.a='A';
    y.x.b='B';
    y.x.c='C';
    y.x.d='D';
    y.x.e='E';
    printf("%ld | %02X.%02X.%02X.%02X.%02X\n", y.adr,y.x.a,y.x.b,y.x.c,y.x.d,y.x.e);
    
}

int main() {

    q16();
    q17();
    q18();
    q19();
    q20();
    q21();
    q22();
    q23();
    q24();
    q25();
    q26();
    q27();

    return 0;
}