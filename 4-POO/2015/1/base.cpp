#include <iostream>
#define __QUESTION__ (1)

using namespace std;

class Base
{
public:
  Base(int v=0):i(v)    { cout << "Cons Base" << endl; }
  virtual void trace()  { cout << "Base::trace() : i=" << i << endl; }
  void execute()        { trace(); }

  virtual Base& plus()  { i++; return *this; }
  Base add()            { i++; return *this; }

  virtual ~Base()       { cout << "Base::~Base()" << endl; }

protected:
  int i;
};


class Derivee : public Base
{
public:
  Derivee(int v=0):Base(v)  { cout << "Cons Derivee" << endl; }
  virtual void trace()      { cout << "Derivee::trace(): i=" << i << endl; }

  virtual Base& plus()      { i = i + 10; return *this; }
  Base add()                { i = i + 10; return *this; }

  ~Derivee()                { cout << "Derivee::~Derivee()" << endl; }
};

int main() {

  if (__QUESTION__ == 1) {
    cout << "-- obj2" << endl;  // -- obj2
    Derivee obj2;               // Cons Base
                                // Cons Derivee
    obj2.plus().plus();
    obj2.execute();             // Derivee::trace(): i=20
                                // Derivee::~Derivee()
                                // Base::~Base()
  }

  if (__QUESTION__ == 2) {
    cout << "-- obj3" << endl;  // -- obj3
    Derivee obj3;               // Cons Base
                                // Cons Derivee
    obj3.add().add();           // Base::~Base()
                                // Base::~Base()
    obj3.execute();             // Derivee::trace() : i=10
                                // Derivee::~Derivee()
                                // Base::~Base()
  }

  if (__QUESTION__ == 3) {
    cout << "-- p" << endl;     // -- p
    Base* p = new Derivee;      // Cons Base
                                // Cons Derivee
    p->plus().plus();
    p->execute();               // Derivee::trace(): i=20
    delete p;                   // Derivee::~Derivee()
                                // Base::~Base()
  }

  if (__QUESTION__ == 4) {
    cout << "-- p3" << endl;    // -- p3
    Base *p3 = new Derivee;     // Cons Base
                                // Cons Derivee
    Base obj5;                  // Cons Base
    obj5 = p3->add().plus();    // Base::~Base()
    p3->execute();              // Derivee::trace() : i=1

    cout << "-- obj5" << endl;  // -- obj5
    obj5.execute();             // Base::trace() : i=2
    delete p3;                  // Derivee::~Derivee()
                                // Base::~Base()
                                // Base::~Base()
  }

  return 0;
}

