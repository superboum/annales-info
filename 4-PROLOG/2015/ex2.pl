grille([1,2,3],[4,3,5],[6,7,8]).

%Question 2.1
pas_de(_, []).
pas_de(H, [T|L]) :-
  H \== T,
  pas_de(H, L).

pas_de_double([]).
pas_de_double([H|L]) :-
  pas_de(H, L),
  pas_de_double(L).

%Question 2.2
toutes_lignes_ok([]).
toutes_lignes_ok([H|L]) :-
  pas_de_double(H),
  toutes_lignes_ok(L).

%Question 2.3
construire([], []).
construire([[H|_]|L], [H |LRes]) :-
  construire(L, LRes).

%construire([], LRes, LRes).
%construire([[H|_]|L], LRes, Acc) :-
%  construire(L, LRes, [H|Acc]).

%Question 2.4
verif_premiere_col(L) :-
  construire(L, LRes),
  pas_de_double(LRes).

%Question 2.5
oter_premiere_col([], []).
oter_premiere_col([[_|R]|L], [R|LRes]) :-
  oter_premiere_col(L, LRes).

%Question 2.6
une_seule_col([]).
une_seule_col([[_|[]]|L]) :-
  une_seule_col(L).

%une_seule_col([[_|[]]|_]). Pourrait faire l'affaire si on a le droit de check que le premier elem.

%Question 2.7
toutes_colonnes_ok(L) :-
  verif_premiere_col(L),
  une_seule_col(L).
toutes_colonnes_ok(L) :-
  verif_premiere_col(L),
  oter_premiere_col(L, M),
  toutes_colonnes_ok(M).

grille_magique(L) :-
  toutes_lignes_ok(L),
  toutes_colonnes_ok(L).
